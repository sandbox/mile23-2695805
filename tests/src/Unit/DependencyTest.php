<?php

/**
 * @file
 * Contains Drupal\Tests\drupalci_d8_module_composer\Unit\DependencyTest.
 */

namespace Drupal\Tests\drupalci_d8_module_composer\Unit;

use Crell\ApiProblem\ApiProblem;

/**
 * Determine whether drupalci_d8_module_composer module can use ApiProblem.
 *
 * @group drupalci_d8_module_composer
 */
class DependencyTest extends \PHPUnit_Framework_TestCase {

  /**
   * Use an ApiProblem object and not throw any exceptions or fatal.
   */
  public function testProblem() {
    $problem = new ApiProblem("Only an expert see it's a problem. Only an expert can deal with a problem.");
    $this->assertNotNull($problem);
  }

}
